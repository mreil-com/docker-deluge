# docker-deluge

[![](https://img.shields.io/bitbucket/pipelines/mreil-com/docker-deluge.svg)](https://bitbucket.org/mreil-com/docker-deluge/addon/pipelines/home)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> [Deluge](https://dev.deluge-torrent.org/) image

Based on [mreil/ubuntu-base][ubuntu-base].

## Usage

    docker run -i -t \
    -p 8112:8112 \
    -v VOLUME1:/root/.config/deluge \
    -v VOLUME2:/root/Downloads/deluge_complete \
    mreil/deluge

### Environment variables

| Variable     | Description                          | Default                |
|--------------|--------------------------------------|------------------------|
| DOWNLOAD_DIR | Incomplete files go here.            | /mnt/deluge-incomplete |
| COMPLETE_DIR | Completely downloaded files go here. | /mnt/deluge-complete   |

## Build

    ./gradlew build

## Releases

See [hub.docker.com][docker-hub-releases].

## License

see [LICENSE](LICENSE)


[ubuntu-base]: https://bitbucket.org/mreil-com/docker-ubuntu-base

[docker-hub-releases]: https://hub.docker.com/r/mreil/deluge/tags/
