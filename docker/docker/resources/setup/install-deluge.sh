#!/usr/bin/env bash

set -e

# Install dependencies
apt-get update -y
apt-get install -y deluged deluge-web deluge-console net-tools gettext-base
apt-get -y autoremove
