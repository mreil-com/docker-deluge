#!/usr/bin/env bash

mkdir -p /root/Downloads

DOWNLOAD_DIR=${DOWNLOAD_DIR:?}
COMPLETE_DIR=${COMPLETE_DIR:?}
DATA_DIR=/root/.config/deluge
SOURCE_DIR=/runtime/conf

# $1: JSON key
# $2: value
replace_vars() {
  if [[ -z "${1}" ]]; then
    echo "Must provide 1 parameters"
    exit 1
  fi
  if [[ ! -f "${1}" ]]; then
    echo Replacing vars in \""${1}"\"
    envsubst < "${SOURCE_DIR}/${1}" > "${DATA_DIR}/${1}"
  else
    echo "File noes not exist: ${1}"
    exit 2
  fi
}

mkdir -p "${DATA_DIR}"

replace_vars core.conf
replace_vars blocklist.conf
replace_vars label.conf

deluged -u 0.0.0.0 -L info -c "${DATA_DIR}" && \
deluge-web -d -L info
