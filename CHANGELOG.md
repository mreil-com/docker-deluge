# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
n/a

## [1.3.2] 2019-02-13
### Added
- Add initial config


## [1.3.1] 2018-10-28
### Added
- First release, contains Deluge 1.3.15

[Unreleased]: ../../src
[1.3.2]: ../../branches/compare/rel-1.3.1..rel-1.3.2#diff
[1.3.1]: ../../src/v1.3.1
